# Spotify auto trim playlist

I tend to listen to a lot of offline musics  and the easiest way to add new musics to the offline cache is adding then to the "All songs" 'playlist' (at least for Android). I don't really need 2000+ musics in offline cache neither my phone has enough sotrage, in fact 50 songs would already be enough. Since managing a playlist is boring this cli will remove old musics from the "All songs" for me whenever I have more musics than a configurable limit.

# TODO

- Handle refuse callback
- Actually do the app

I'm using this project to learn Rust while solving a personal problem with Spotify.

## Dependencies

Tried to avoid using much dependencies to learn more about rust/cargo (like avoid using a 'full' web server and handcoding http responses).

### open

Opens the spotify authentication form in the user's browser. The original idea was to open the form with a webpage to have better flow control ( for instance, detecting if the user closed the page before submitting the form ). Most webview libs I tried didn't support opening a URL or had weird bugs ( for windows at least ) like this one:

https://github.com/Boscop/web-view/issues/3 (It's probably related since the app crashes whenever the webpage is programatically terminated, even with the lib samples)

There is no need to use specific visual studio native tools to compile for windows or a windows developer license and a UWP manifest...

Another gain for not using webpage was deleting all the multithread support since it wasn't necessary anymore which simplified the code.

## url

Does a lot of URL parsing/validating for us.

## dotenv

Load the .env file in the OS envrionment. The spotify credentials and configs are there.

## quicli

This automates the boring stuff of the cli and logging. Unfortunatelly it does weird stuff like aliasing the `Result` type or forcing to use `prelude::*` if you wish to keep the macros.

## uuid

Used to generate random uuids (thus only depending on the v4) for the spotify authentication login form.

## base64

Spotify uses Basic http authentication for requesting the access token

## hyper

HTTP Client/Server ( altough only the client is used). Does all requests to spotify after we have a authentication code.

### hyper-tls

HTTPS Connector since the default hyper does not supports tls.
