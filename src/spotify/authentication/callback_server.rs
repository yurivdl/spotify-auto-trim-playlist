/// Starts a simple webserver to wait for spotify authorization callback
/// The authorization should be requested by the auth_webview module.

//The use is here for quicli log macros
#[allow(unused_imports)]
use quicli::prelude::*;
//And this to avoid that painfull Result alias that quicli does
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
use std::result::Result;

use open;
use url::{HostAndPort, Url};

use super::spotify_auth;

const CALLBACK_HTML: &'static [u8] = include_bytes!("callback.html");

///Callback for Url::with_default_port
fn default_port(url: &Url) -> Result<u16, ()> {
    match url.scheme() {
        "https" => Ok(443),
        "http" => Ok(80),
        _ => Err(()),
    }
}

// Parses the raw url to a socket Url
/// #PANICS
///
/// If the spotify callback url is invalid (is unparseable, base, has no host or cannot be used as a socket address)
fn get_spotify_callback_url(callback_url: String) -> HostAndPort {
    let url = match Url::parse(&callback_url) {
        Ok(url) => url,
        Err(e) => panic!(
            "The spotify callback cannot be parsed: {} {}",
            callback_url, e
        ),
    };

    //Checking for http(s) may already validate if the url is a base, but I'm not sure
    if url.cannot_be_a_base() || !url.has_host()
        || (url.scheme() != "http" && url.scheme() != "https")
    {
        panic!("The spotify callback url is invalid \"{}\" ", callback_url);
    }

    let tcp_url = match url.with_default_port(default_port) {
        Ok(_tcp_url) => _tcp_url.to_owned(),
        Err(e) => panic!(
            "Spotify callback uri is not a valid SockAddr: {} {}",
            callback_url, e
        ),
    };

    return tcp_url;
}

/// Starts a simple local webserver to receive the spotify response.
/// If the webserver starts successfully it will spawn a webpage with the spotify
/// authorization form that the user must fill
///
/// By the end of this function the auth.set_spotify_auth_code or the auth.set_spotify_auth_error should be setted
///
/// #PANICS
///
/// If the spotify callback url is invalid (is unparseable, base, has no host or cannot be used as a socket address)
/// Panic if the callback server fails to listen for the spotify callback port (missing privileges or the port is already bound)
///
pub fn start_server(mut auth: &mut spotify_auth::SpotifyAuth) {
    info!("Starting callback server");

    let tcp_url = get_spotify_callback_url(auth.app_callback_url.to_owned());

    let sock_listener = match TcpListener::bind(&tcp_url) {
        Ok(listener) => listener,
        Err(e) => panic!(
            "Could not start webserver at address {} with error: {} ",
            tcp_url, e
        ),
    };

    open_spotify_auth(&auth);

    info!("Listening for spotify callback at addr: {}", tcp_url);
    for stream in sock_listener.incoming() {
        debug!("Received a connection from the callback");

        match stream {
            Ok(mut tcp_stream) => {
                //Since this is a short lived webserver (should be at least) there is no need to turn this into a separate thread.
                handle_callback_connection(&mut auth, tcp_stream);
                //TODO - close the webserver.. ?
                if auth.get_received_code() {
                    break;
                }
            }
            Err(e) => error!("Callback tcp stream failed with error: {}", e),
        }
    }
}

fn handle_callback_connection(auth: &mut spotify_auth::SpotifyAuth, mut stream: TcpStream) {
    let mut buffer = vec![0; 512];
    let mut header = "".to_string();

    loop {
        match stream.read(&mut buffer) {
            Ok(size) => {
                debug!("Bytes reading from tcp: {}", size);
                if 0 == size {
                    break;
                }

                header.push_str(&String::from_utf8_lossy(&buffer[..]));
                buffer.iter_mut().map(|v| *v = 0).count();

                if size < buffer.len() {
                    break;
                }
            }
            Err(e) => {
                warn!("Failed to read data from tcp stream: {}", e);
                break;
            }
        }
    }

    let response = format!(
        "HTTP/1.1 200 OK\r\n\r\n{}",
        String::from_utf8_lossy(CALLBACK_HTML)
    );

    //I guess it is possible to ignore writing/flushing errors since we already got at we wanted but to avoid
    //hard to catch bugs its better to log those unpredicted errors.
    match stream.write(response.as_bytes()) {
        Err(e) => warn!("Failed to write response to tcp stream: {}", e),
        _ => (),
    }

    match stream.flush() {
        Err(e) => warn!("Failed to flush tcp stream: {}", e),
        _ => (),
    }

    if let Some(url_query) = fetch_callback_query(header) {
        if !auth.read_callback_query_into(url_query) {
            warn!("Received a invalid spotify callback response, invalid state param");
        }
    } else {
        warn!("Received a invalid spotify callback response");
    }
}

/// This methods finds the query data from the requests and parse it into a Url struct
/// requests data example: GET /something=1&otherthing HTTP/1.1\r\n...
fn fetch_callback_query(request: String) -> Option<Url> {
    info!("Received callback request!");
    debug!("Request: {}", request);

    let get_method_lim = "GET /";
    let get_query_lim = " HTTP/";

    let request_method_ind = request.find(get_method_lim);
    let request_query_ind = request.find(get_query_lim);

    if request_method_ind.is_none() || request_query_ind.is_none() {
        return None;
    }

    //Unwrap here should be safe since we checked for None before.
    let request_i_offset = request_method_ind.unwrap();
    let request_e_offset = request_query_ind.unwrap();

    debug!("The method byte offset is {}", request_i_offset);

    // I know the '/' is only 1 byte.
    let method_drain_len = get_method_lim.len() - 1;

    let mut query_params: String = request.to_string();

    query_params.drain(..(request_i_offset + method_drain_len));
    let mut raw_query: String = query_params
        .drain(..(request_e_offset - method_drain_len))
        .collect();

    raw_query.insert_str(0, "http://fake.base");

    let url = match Url::parse(&raw_query) {
        Ok(url) => url,
        Err(e) => {
            warn!("Failed to parse spotify url '{}' : {}", raw_query, e);
            return None;
        }
    };

    return Some(url);
}

/// Will try to open the spotify login form in the user browser.
fn open_spotify_auth(auth: &spotify_auth::SpotifyAuth) {
    if let Err(e) = open::that(auth.get_auth_form_url()) {
        warn!("There was an error trying to open spotify login form in your browser but ! You can open manually with the link: {}", "");
        debug!("{}", e);
    };
}
