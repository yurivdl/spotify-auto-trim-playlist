///Contains the SpotifyAuth struct.

//The use is here for quicli log macros
#[allow(unused_imports)]
use quicli::prelude::*;

use std::env;
use uuid::Uuid;

use base64;
use url::Url;

const CLIENT_ID_KEY: &str = "SPOTIFY_CLIENT_ID";
const CLIENT_SECRET_KEY: &str = "SPOTIFY_CLIENT_SECRET";
const CALLBACK_URI_KEY: &str = "SPOTIFY_CALLBACK";

///This structs represents a authentication request for Spotify
///This is also the result of a call to spotify::authentication::request()
pub struct SpotifyAuth {
    /// Spotify's app client id found in the app dashboard
    pub app_client_id: String,
    /// Spotify's app client secret also found in the app dashboard
    app_client_secret: String,
    /// Spotify's app valid callback registered in the app dashboard (Edit Settings -> Redirect URIs)
    pub app_callback_url: String,
    /// Spotify's callback code. Used to request the auth token.
    spotify_auth_code: Option<String>,
    /// Spotify's callback error message. If the user denied access or some other error happened it will be here.
    /// Usually the auth_error should be None when the auth_code is received.
    spotify_auth_error: Option<String>,
    /// Spotify's login form state. Used to validated if the callback we received was from spotify.
    auth_param_state: Uuid,
    received_code: bool,
}

impl SpotifyAuth {
    /// Searchs the OS env for the spotify app client id and secret
    /// #PANICS
    ///
    /// If any key from environment is missing the struct panics.   
    pub fn new() -> SpotifyAuth {
        info!("Fetching spotify server side auth");
        let mut auth: SpotifyAuth = SpotifyAuth {
            app_client_id: String::from(""),
            app_client_secret: String::from(""),
            app_callback_url: String::from(""),
            auth_param_state: Uuid::new_v4(),
            spotify_auth_code: None,
            spotify_auth_error: None,
            received_code: false,
        };

        match env::var(CLIENT_ID_KEY) {
            Ok(val) => auth.app_client_id = val,
            Err(e) => panic!(
                "Missing Spotify's app client id in the envrionment! {}: {}",
                CLIENT_ID_KEY, e
            ),
        }

        debug!("Client id: {}", auth.app_client_id);

        match env::var(CLIENT_SECRET_KEY) {
            Ok(val) => auth.app_client_secret = val,
            Err(e) => panic!(
                "Missing Spotify's app client secret in the envrionment! {}: {}",
                CLIENT_SECRET_KEY, e
            ),
        }

        debug!("Client Secret: {}", auth.app_client_secret);

        match env::var(CALLBACK_URI_KEY) {
            Ok(val) => auth.app_callback_url = val,
            Err(e) => panic!(
                "Missing Spotify's app calllback in the envrionment! {}: {}",
                CALLBACK_URI_KEY, e
            ),
        }

        debug!("Callback uri: {}", auth.app_callback_url);

        return auth;
    }

    /// Returns a formated url for authentication with spotify.
    pub fn get_auth_form_url(&self) -> String {
        return format!(
        "https://accounts.spotify.com/authorize?show_dialog=false&scope=user-read-private&response_type=code&client_id={}&redirect_uri={}&state={}",
        self.app_client_id, self.app_callback_url, self.auth_param_state);
    }

    /// Returns the body in the format application/x-www-form-urlencoded for the token request.
    /// #PANICS
    ///
    /// If the spotify_auth_code wasn't set
    pub fn get_token_access_body(&self) -> String {
        //TODO - unfuck this
        return format!(
            "grant_type=authorization_code&code={}&redirect_uri={}",
            self.spotify_auth_code.to_owned().unwrap(),
            self.app_callback_url
        );
    }

    /// Iterate through the query params to figure out how it went.
    /// Returns false if the response was not sent from spotify.
    pub fn read_callback_query_into(&mut self, url: Url) -> bool {
        let mut pairs = url.query_pairs();
        let mut state_passes = false;

        while let Some(pair) = pairs.next() {
            let (key, val) = pair;

            debug!("process_callback_query at key {} with val {} .", key, val);

            if key == "state" && val == self.auth_param_state.to_string() {
                state_passes = true;
            //TODO - If the state does not passes and is last in the query other fields may be populated.
            } else if key == "code" {
                self.set_spotify_auth_code(val.to_string());
            } else if key == "error" {
                self.set_spotify_auth_error(val.to_string());
            }
        }

        debug!("Finished process_callback_query");

        return state_passes;
    }

    /// Returns true if the struct received a code from spotify
    /// The members self.spotify_auth_error or self.spotify_auth_code will be populated.
    /// If spotify_auth_error is set then spotify returned with an error and the message is contained in the variable.
    /// If spotify_auth_code is set then it can be used to fetch the auth token and expiration token
    pub fn get_received_code(&self) -> bool {
        return self.received_code;
    }

    pub fn get_auth_code(&self) -> Option<String> {
        return self.spotify_auth_code.to_owned();
    }

    pub fn get_auth_error(&self) -> Option<String> {
        return self.spotify_auth_error.to_owned();
    }

    //Returns client_id:client_secret encoded in Base64
    pub fn get_cli_and_secret_b64(&self) -> String {
        return base64::encode(&format!(
            "{}:{}",
            self.app_client_id, self.app_client_secret
        ));
    }

    fn set_spotify_auth_code(&mut self, code: String) {
        if !self.received_code {
            self.spotify_auth_code = Some(code.to_owned());
            self.received_code = true;
        } else {
            debug!("Ignoring set_spotify_auth_code called after received_code was set to true.");
        }
    }

    fn set_spotify_auth_error(&mut self, message: String) {
        if !self.received_code {
            self.spotify_auth_error = Some(message.to_owned());
            self.received_code = true;
        } else {
            debug!("Ignoring set_spotify_auth_error called after received_code was set to true.");
        }
    }
}
