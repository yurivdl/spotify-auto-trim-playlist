/**
 * This module will handle asking the user for authorization
 * and return the auth token if succesfull
*/
mod callback_server;
mod fetch_token;
mod spotify_auth;

/// Block execution until the user's finish autorizing with spotify's form.
pub fn fetch_access_token() -> Option<String> {
    let mut auth = spotify_auth::SpotifyAuth::new();

    //TODO - Add support for spotify show_dialog parameter (-f --force on the cli?)
    callback_server::start_server(&mut auth);
    fetch_token::fetch_tokens(auth);

    return None;
}
