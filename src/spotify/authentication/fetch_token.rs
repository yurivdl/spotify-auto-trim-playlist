/// Consumes SpotifyAuth.spotify_auth_code to fetch the access adn refresh tokens
use hyper::rt::{self, Future, Stream};
use hyper::{Body, Client, Method, Request, Uri};
use hyper_tls::HttpsConnector;
use std::io::{self, Write};

use super::spotify_auth::SpotifyAuth;

const SPOTIFY_ACCESS_URL: &'static str = "https://accounts.spotify.com/api/token";

/// #PANICS
///
/// If it fails to parse the spotify url in the const `spotify_access_url`
pub fn fetch_tokens(auth: SpotifyAuth) {
    //let url = spotify_access_url.parse::<Uri>().unwrap();

    let url: Uri = match SPOTIFY_ACCESS_URL.parse() {
        Ok(url) => url,
        Err(e) => panic!("Failed to parse spotify URL: {} {}", SPOTIFY_ACCESS_URL, e),
    };

    let req_body = Body::from(auth.get_token_access_body());
    let mut req = Request::new(req_body);

    *req.method_mut() = Method::POST;
    *req.uri_mut() = url.clone();

    {
        let headers = req.headers_mut();

        headers.insert(
            "Content-Type",
            //The parse() will call HttpValues::from_str
            "application/x-www-form-urlencoded".parse().unwrap(),
        );

        headers.insert(
            "Authorization",
            format!("Basic {}", auth.get_cli_and_secret_b64())
                .parse()
                .unwrap(),
        );
    }

    let post = build_post(req);

    rt::run(post);
}

fn build_post(req: Request<Body>) -> impl Future<Item = (), Error = ()> {
    let https = HttpsConnector::new(4).unwrap();
    let client = Client::builder().build::<_, Body>(https);

    client.request(req)
    .and_then(|res| {
            println!("Response: {}", res.status());
            println!("Headers: {:#?}", res.headers());

            // The body is a stream, and for_each returns a new Future
            // when the stream is finished, and calls the closure on
            // each chunk of the body...
            // println!("{:?}", String::from_utf8_lossy(body));
            res.into_body().for_each(|chunk| {
                io::stdout()
                .write(String::from_utf8_lossy(&chunk.to_vec()).as_bytes()).unwrap();
                
                io::stdout().write_all(&[])
                   .map_err(|e| panic!("example expects stdout is open, error={}", e))
            })//TODO deal with response error and such...
        })
        // If all good, just tell the user...
        .map(|_| {
            println!("\n\nDone.");
        })
        // If there was an error, let the user know...
        .map_err(|err| {
            eprintln!("Error {}", err);
        })
}
