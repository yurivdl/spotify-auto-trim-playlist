use quicli::prelude::*;

/// Trim a playlist removing old songs based date added.
#[derive(Debug, StructOpt)]
pub struct Cli {
    /// Limit how many musics the playlist can contain until they are trimmed
    #[structopt(long = "limit", short = "l", default_value = "100")]
    pub limit: usize,
    #[structopt(
        long = "remove-not-available",
        short = "r",
        help = "Songs not avaiable will be removed from the playlist before trimming"
    )]
    pub remove_not_available: bool,
    /// Spotify username. The
    pub username: String,
    // Quick and easy logging setup you get for free with quicli
    // Adds the -vv option
    #[structopt(flatten)]
    pub verbosity: Verbosity,
}
