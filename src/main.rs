#[macro_use]
extern crate quicli;

extern crate base64;
extern crate dotenv;
extern crate hyper;
extern crate hyper_tls;
extern crate open;
extern crate url;
extern crate uuid;

use dotenv::dotenv;
use quicli::prelude::*;

mod cli;
mod spotify;

main!(|args: cli::Cli, log_level: verbosity| {
    dotenv().ok();

    info!(".env loaded.");

    println!("Checking for authentication with Spotify. A browser page should open with the login form. If someone already logged the redirect page will open instead.");
    //let auth =
    spotify::authentication::fetch_access_token();

    // if let Some(c) = auth.get_auth_code() {
    //     println!("Spotify auth received: {}", c);
    // } else {
    //     println!("NAY!");
    // }
});
